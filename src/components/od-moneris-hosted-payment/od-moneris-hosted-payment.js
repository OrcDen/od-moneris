const template = document.createElement( "template" );
template.innerHTML = `
    <style></style>
    
    <div class="moneris-hosted-payment">
        <form method='POST' id='moneris-form'>
            <input type="hidden" name="ps_store_id">
            <input type="hidden" name="hpp_key">
            <input type="hidden" name="order_id">
            <input type="hidden" name="cust_id">
            <input type="hidden" name="email">
            <input type="hidden" name="note">
            <input type="hidden" name="charge_total">
            <input type="hidden" name="hst">
        </form>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-moneris-hosted-payment" );

export class OdMonerisHostedPayment extends HTMLElement {
    constructor() {
        super();
        this._ready = false;
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._moneris = this.shadowRoot.querySelector( "#moneris-form" );
        this._storeIdInput = this.shadowRoot.querySelector( 'input[name="ps_store_id"]' );
        this._hppKeyInput = this.shadowRoot.querySelector( 'input[name="hpp_key"]' );
        this._orderIdInput = this.shadowRoot.querySelector( 'input[name="order_id"]' );
        this._customerNameInput = this.shadowRoot.querySelector( 'input[name="cust_id"]' );
        this._customerEmailInput = this.shadowRoot.querySelector( 'input[name="email"]' );
        this._noteInput = this.shadowRoot.querySelector( 'input[name="note"]' );
        this._chargeTotalInput = this.shadowRoot.querySelector( 'input[name="charge_total"]' );
        this._hstInput = this.shadowRoot.querySelector( 'input[name="hst"]' );        

        //set attribute default values first and pass to element
        if( !this.hasAttribute( 'hpp-endpoint' ) ) {
            this.setAttribute( 'hpp-endpoint', "" );
        }
        if( !this.hasAttribute( 'store-id' ) ) {
            this.setAttribute( 'store-id', "" );
        }
        if( !this.hasAttribute( 'hpp-key' ) ) {
            this.setAttribute( 'hpp-key', "" );
        }
        if( !this.hasAttribute( 'order-id' ) ) {
            this.setAttribute( 'order-id', "" );
        }
        if( !this.hasAttribute( 'customer-name' ) ) {
            this.setAttribute( 'customer-name', "" );
        }
        if( !this.hasAttribute( 'customer-email' ) ) {
            this.setAttribute( 'customer-email', "" );
        }
        if( !this.hasAttribute( 'note' ) ) {
            this.setAttribute( 'note', "" );
        }
        if( !this.hasAttribute( 'charge-total' ) ) {
            this.setAttribute( 'charge-total', "" );
        }
        if( !this.hasAttribute( 'hst' ) ) {
            this.setAttribute( 'hst', "" );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'hppEndpoint' );
        this._upgradeProperty( 'storeId' );
        this._upgradeProperty( 'hppKey' );
        this._upgradeProperty( 'orderId' );
        this._upgradeProperty( 'customerName' );
        this._upgradeProperty( 'customerEmail' );
        this._upgradeProperty( 'note' );
        this._upgradeProperty( 'chargeTotal' );
        this._upgradeProperty( 'hst' );

        //listeners and others etc.


    } 

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ['hpp-endpoint','store-id','hpp-key','order-id','customer-name','customer-email','note','charge-total','hst'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "hpp-endpoint": {
                if( newValue !== oldValue ) {                    
                    this._setHppEndpoint( newValue );
                }
                break;
            }
            case "store-id": {
                if( newValue !== oldValue ) {
                    this._setStoreId( newValue );
                }
                break;
            }
            case "hpp-key": {
                if( newValue !== oldValue ) {
                    this._setHppKey( newValue );
                }
                break;
            }
            case "order-id": {
                if( newValue !== oldValue ) {
                    this._setOrderId( newValue );
                }
                break;
            }
            case "customer-name": {
                if( newValue !== oldValue ) {
                    this._setCustomerName( newValue );
                }
                break;
            }
            case "customer-email": {
                if( newValue !== oldValue ) {
                    this._setCustomerEmail( newValue );
                }
                break;
            }
            case "note": {
                if( newValue !== oldValue ) {
                    this._setNote( newValue );
                }
                break;
            }
            case "charge-total": {
                if( newValue !== oldValue ) {
                    this._setChargeTotal( newValue );
                }
                break;
            }
            case "hst": {
                if( newValue !== oldValue ) {
                    this._setHst( newValue );
                }
                break;
            }
        }
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////
    
    get hppEndpoint() {
        return this.getAttribute( "hpp-endpoint" );
    }

    set hppEndpoint( endpoint ) {        
        this.setAttribute( 'hpp-endpoint', endpoint );
    }

    _setHppEndpoint( endpoint ) {
        this.setAttribute( 'hpp-endpoint', endpoint );
        this._moneris.setAttribute( 'action', endpoint )
    }
    
    get storeId() {
        return this.getAttribute( "store-id" );
    }

    set storeId( id ) {
        this.setAttribute( 'store-id', id );
    }
    
    _setStoreId( id ) {
        this.setAttribute( 'store-id', id );
        this._storeIdInput.value = id;
    }
    
    get hppKey() {
        return this.getAttribute( "hpp-key" );
    }

    set hppKey( key ) {
        this.setAttribute( 'hpp-key', key );
    }
    
    _setHppKey( key ) {
        this.setAttribute( 'hpp-key', key );
        this._hppKeyInput.value = key;
    }
    
    get orderId() {
        return this.getAttribute( "order-id" );
    }

    set orderId( id ) {
        this.setAttribute( 'order-id', id );
    }
    
    _setOrderId( id ) {
        this.setAttribute( 'order-id', id );
        this._orderIdInput.value = id;
    }
    
    get customerName() {
        return this.getAttribute( "customer-name" );
    }

    set customerName( name ) {
        this.setAttribute( 'customer-name', name );
    }
    
    _setCustomerName( name ) {
        this.setAttribute( 'customer-name', name );
        this._customerNameInput.value = name;
    }
    
    get customerEmail() {
        return this.getAttribute( "customer-email" );
    }

    set customerEmail( email ) {
        this.setAttribute( 'customer-email', email );
    }
    
    _setCustomerEmail( email ) {
        this.setAttribute( 'customer-email', email );
        this._customerEmailInput.value = email;
    }
    
    get note() {
        return this.getAttribute( "note" );
    }

    set note( note ) {
        this.setAttribute( 'note', note );
    }
    
    _setNote( note ) {
        this.setAttribute( 'note', note );
        this._noteInput.value = note;
    }
    
    get chargeTotal() {
        return Number( this.getAttribute( "charge-total" ) ).toFixed( 2 );
    }

    set chargeTotal( total ) {
        this.setAttribute( 'charge-total', total );
    }
    
    _setChargeTotal( total ) {
        this.setAttribute( 'charge-total', total );
        this._chargeTotalInput.value = Number( total ).toFixed( 2 );
    }
    
    get hst() {
        return Number( this.getAttribute( "hst" ) ).toFixed(2);
    }

    set hst( hst ) {
        this.setAttribute( 'hst', hst );
    }
    
    _setHst( hst ) {
        this.setAttribute( 'hst', hst );
        this._hstInput.value = Number( hst ).toFixed( 2 );
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    submit() {
        this._moneris.submit();
    }
}