# <od-moneris-hosted-payment>

> A component to easily connect to a moneris hosted endpoint

`<od-moneris-hosted-payment>` is a simple component that can be configured using the regular moneris hosted endpoint configs. This component just wraps the form in an easy to use interface.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-moneris-hosted-payment
```
## Usage
```
import '@orcden/od-moneris-hosted-payment';
```
```
<od-moneris-hosted-payment></od-moneris-hosted-payment>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `hpp-endpoint`  | String | ""   | The moneris hosted endpoint you are connecting to |
| `store-id`  | String | ""   | The moneris store ID you are connecting to |
| `hpp-key`  | String | ""   | The moneris hosted endpoint key you are connecting to |
| `order-id`  | String | ""   | The orderId you would like to attach to the moneris order |
| `customer-name`  | String | ""   | The customers name that is making a purchase |
| `customer-email`  | String | ""   | The customers email that is making a purchase |
| `note`  | String | ""   | A simple note you would like to attach to the order |
| `charge-total`  | String | ""   | The Total including tax that is being charged |
| `hst`  | String | ""   | The HST portion of the order |

## Properties
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `hppEndpoint`  | String | ""   | The moneris hosted endpoint you are connecting to |
| `storeId`  | String | ""   | The moneris store ID you are connecting to |
| `hppKey`  | String | ""   | The moneris hosted endpoint key you are connecting to |
| `orderId`  | String | ""   | The orderId you would like to attach to the moneris order |
| `customerName`  | String | ""   | The customers name that is making a purchase |
| `customerEmail`  | String | ""   | The customers email that is making a purchase |
| `note`  | String | ""   | A simple note you would like to attach to the order |
| `chargeTotal`  | String | ""   | The Total including tax that is being charged |
| `hst`  | String | ""   | The HST portion of the order |


## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `submit`   | None | submits the form and redirects the user to a new page hosteed by moneris |

## Styling
- None

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```